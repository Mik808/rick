import { SearchedCharacter} from '../GeneralTypes';

export interface CharacterCardProps {
    onCLickCard: (character: SearchedCharacter) => void;
    onClose: ((e: React.MouseEvent<HTMLElement>, id:string) => void);
    showCloseBtn: Boolean;
    character: SearchedCharacter
}