import { SearchedCharacters } from '../../Types/GeneralTypes';

export interface PartyProps {
    characters?: SearchedCharacters
}