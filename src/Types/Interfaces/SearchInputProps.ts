import { ChangeE } from '../../Types/GeneralTypes';

export interface SearchInputProps {
    onchange: (e: ChangeE) => void;
}