export interface SearchedCharacter {
    __typename: string,
    name: string,
    image: string
    id: string
}

export interface SearchedCharacters extends Array<SearchedCharacter>{}

export type ChangeE = React.ChangeEvent<HTMLInputElement> 
export type MouseE = React.MouseEvent<HTMLElement>;
// React.MouseEvent<HTMLDivElement>
// React.MouseEvent<HTMLElement>

