import { createGlobalStyle } from 'styled-components';

import RobotoRegular from './Roboto/Roboto-Regular.woff'

const FontStyles = createGlobalStyle`
    @font-face {
        font-family: 'RobotoRegular';
        src: url(${RobotoRegular}) format('woff');
    }
`;

export default FontStyles;