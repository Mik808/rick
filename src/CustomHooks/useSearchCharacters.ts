import { useState, useEffect, useCallback } from "react"
import { useLazyQuery } from "@apollo/client";
import { FIND_CHARACTERS } from "../GraphQL/Queries";
import { SearchedCharacters, ChangeE } from '../Types/GeneralTypes';
import { throttle } from 'lodash'

export default function useSearchCharacters() {
    const [name, setName] = useState("");
    const [characters, setCharacters] = useState<SearchedCharacters | null>([]);
    // console.log('useSearchCharacters', characters);
    const [search, { loading, error, data }] = useLazyQuery(FIND_CHARACTERS, { variables: { name: name } })
    const throttledSearch = throttle(search, 300);
    const throttledSearchCallback = useCallback(throttledSearch, []);

    // console.log('useSearchCharacters data', data)
    const handleSearch = (e: ChangeE) => {
        if (e.target.value.length > 2) {
            setName(e.target.value);
        }
    }

    useEffect(() => {
        if (name) {
            throttledSearchCallback();
        }
    }, [name]);

    useEffect(() => {
        if (data) {
            setCharacters(data.characters?.results);
        }
    }, [data]);

    return { characters, loading, error, handleSearch };
}