import { useState } from "react"
import { SearchedCharacters, SearchedCharacter, MouseE } from '../Types/GeneralTypes';

export default function useExcludedcharacters () {
    const [pickedCharacters, setPickedCharacters] = useState<SearchedCharacters>([]);
    const [exlucdedCharacters, setExcludeCharacters] = useState<Array<string>>([]);

    const pickCharater = (char:SearchedCharacter) => {
        // console.log("pickCharater", char)
        const partyIds = ["1", "2"];
        if (char.id === partyIds[0] ) {
            let updatedChars = [...pickedCharacters]
            updatedChars[0] = char;
            setPickedCharacters(updatedChars) 
        }
        if (char.id === partyIds[1] ) {
            let updatedChars = [...pickedCharacters]
            updatedChars[1] = char;
            setPickedCharacters(updatedChars) 
        }
    }

    const deleteCahracter = (e:MouseE, id:string) => {
        e.stopPropagation();
        setExcludeCharacters([...exlucdedCharacters, id]); 
    }

    const filterCharaters = (characters:SearchedCharacters | null) => {
        if (exlucdedCharacters.length) {
            return characters?.filter( char => exlucdedCharacters.indexOf(char.id) === -1 )
        } else {
            return characters
        }
    }

    return {pickedCharacters, pickCharater, deleteCahracter,filterCharaters}
}
