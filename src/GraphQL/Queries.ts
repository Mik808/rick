import { gql } from "@apollo/client";

export const FIND_CHARACTERS = gql`
  query FindCharacters ($name: String!) {
      characters(filter: { name: $name }) {
        results {
          name,
          image,
          id
        }
      }
    }
`;