import React from 'react';
import './App.css';
import PartyPage from './components/PartyPage/PartyPage';
import FontStyles from "./assets/fonts/fonts";

function App() {
  return (
    <div>
      <FontStyles />
      <PartyPage />
    </div>
  );
}

export default App;
