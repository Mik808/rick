import loader from '../../assets/images/rhombus.gif';

export default function Preloader() {
    return (
        <img src={loader} alt="loading"/>
    )
}