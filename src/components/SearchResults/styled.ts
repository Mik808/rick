import styled from 'styled-components';

export const StyledSearchResults = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    column-gap: 30px;
    margin: 30px 0;
    min-height: 60px;
`;

export const StatusBlock = styled.div`
    height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
`

