import { useState } from 'react';
import { SearchedCharacter, SearchedCharacters, MouseE } from '../../Types/GeneralTypes';
import { StyledSearchResults, StatusBlock } from './styled';
import CharacterCard from '../CharacterCard/CharacterCard';
import Preloader from '../Preloader/Preloader';

interface SearchResultsProps {
    results: SearchedCharacters | null | undefined
    onClickCard: (character: SearchedCharacter) => void;
    onDelete: (e: MouseE, id: string) => void;
    error?: object;
    loading: Boolean;
}

export default function SearchResults(props: SearchResultsProps) {
    // console.log("props.error", props.error)
    // console.log("top props.results", props.results)

    const renderResults = () => {
        if (props.loading) {
            return (
                <StatusBlock>
                    <Preloader />
                </StatusBlock>
            )
        }

        if (!props.results || props.error) {
            return <StatusBlock>Nothing found</StatusBlock>
        }

        return (
            props.results?.map(character => {
                return (
                    <CharacterCard
                        key={character.id}
                        onCLickCard={props.onClickCard}
                        onClose={props.onDelete}
                        showCloseBtn={true}
                        character={character}
                    />
                )
            })
        )
    }

    return (
        <StyledSearchResults>
            {renderResults()}
        </StyledSearchResults>
    )
}