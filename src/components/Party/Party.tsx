import { EmptyCharacterCard, StlyedParty, StyledImg, Title, PartyBody } from './styled';
import { SearchedCharacter } from '../../Types/GeneralTypes';
import { PartyProps } from '../../Types/Interfaces/PartyProps';

export default function Party(props: PartyProps) {
    // console.log("props.characters", props.characters)

    const rick: SearchedCharacter | undefined = props.characters ? props.characters[0] : undefined;
    const morty: SearchedCharacter | undefined = props.characters ? props.characters[1] : undefined;

    return (
        <StlyedParty>
            <Title>Party</Title>
            <PartyBody>
                {rick ? <StyledImg src={rick.image} /> : <EmptyCharacterCard>Rick</EmptyCharacterCard>}
                {morty ? <StyledImg src={morty.image} /> : <EmptyCharacterCard>Morty</EmptyCharacterCard>}
            </PartyBody>
        </StlyedParty>
    )
}