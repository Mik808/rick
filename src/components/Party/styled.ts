import styled from 'styled-components';

export const StlyedParty = styled.div`
    margin-top: 15px;
    text-align: center;
`;

export const PartyBody = styled.div`
    display: flex;
    justify-content: center;
`

export const EmptyCharacterCard = styled.div`
    width: 180px;
    height: 220px; 
    padding: 165px 0 14px 0;
    background-color: #DADADA;
    font-size: 24px;
    color: #fff;
    text-transform: uppercase;
    text-align: center;
    box-sizing: border-box;
    margin: 0 15px;
`;

export const StyledImg = styled.img`
    width: 180px; 
    height: 220px;
    margin: 0 15px;
`
export const Title = styled.h2 `
    font-weight: 700;
    font-size: 30px;
    margin-bottom: 20px;
    text-transform: uppercase;
`