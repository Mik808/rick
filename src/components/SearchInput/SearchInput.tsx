import { InputField } from './styled'
import { SearchInputProps } from '../../Types/Interfaces/SearchInputProps'

export default function SearchInput(props: SearchInputProps) {

    return (
        <InputField type="text" onChange={props.onchange} />
    )
}