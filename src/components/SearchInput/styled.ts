import styled from 'styled-components';

export const InputField = styled.input`
    height: 80px;
    font-size: 30px;
    text-transform: uppercase;
    width: 100%;
    box-sizing: border-box;
    font-family: inherit;
`;