import { CharacterCardStyled, Cross, Image } from './styled';
import { MouseE } from '../../Types/GeneralTypes';
import {CharacterCardProps } from '../../Types/Interfaces/CharacterCardProps'

export default function CharacterCard(props: CharacterCardProps) {

    return (
        <CharacterCardStyled key={props.character.id} onClick={() => props.onCLickCard(props.character)}>
            {props.showCloseBtn ? <Cross onClick={(e: MouseE) => props.onClose(e, props.character.id)}><div>✕</div></Cross> : null}
            <Image src={props.character.image} />
        </CharacterCardStyled>
    )
}