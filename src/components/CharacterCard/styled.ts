import styled from 'styled-components';

export const CharacterCardStyled = styled.div`
    cursor: pointer;
    width: 180px;
    height: 220px;
    margin: 15px 0;
    position: relative;
`;

export const Image = styled.img`
    width: 180px;
    height: 220px;
`;

export const Cross = styled.div`
    width: 30px;
    height: 30px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    line-height: 30px;
    background-color: #fff;
    position: absolute;
    top: 8px;
    right: 8px;

    &:hover {
        background-color: #f2f2f2;
    }
`;

