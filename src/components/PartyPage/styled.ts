import styled from 'styled-components';

export const StyledPartyPage = styled.div`
    max-width: 810px;
    margin: 20px auto;
    font-family: "RobotoRegular", serif;
`;