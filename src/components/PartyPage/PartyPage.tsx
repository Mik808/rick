import SearchResults from '../SearchResults/SearchResults';
import Party from '../Party/Party';
import SearchInput from '../SearchInput/SearchInput';
import { StyledPartyPage } from './styled'
import useSearchCharacters from '../../CustomHooks/useSearchCharacters'
import useExcludedcharacters from '../../CustomHooks/useExcludedcharacters'

export default function PartyPage() {
    const {characters, loading, error, handleSearch } = useSearchCharacters();
    const {pickedCharacters, pickCharater, deleteCahracter, filterCharaters} = useExcludedcharacters();

    const filterCharacters = filterCharaters(characters);

    return (
        <StyledPartyPage>
            <SearchInput onchange={handleSearch} />
            <SearchResults results={filterCharacters} onClickCard={pickCharater} onDelete={deleteCahracter} loading={loading} error={error} />
            <Party characters={pickedCharacters} />
        </StyledPartyPage>
    )
}